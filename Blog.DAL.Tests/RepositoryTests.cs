﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using Blog.DAL.Infrastructure;
using Blog.DAL.Model;
using Blog.DAL.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using TDD.DbTestHelpers.Core;

namespace Blog.DAL.Tests
{
	[TestClass]
	public class RepositoryTests : DbBaseTest<BlogFixtures>
	{
		[TestMethod]
		public void DoNothing_ShouldAlwaysPass()
		{
			Assert.IsTrue(true);
		}

		[TestMethod]
		public void GetAllPost_OnePostInDb_ShouldReturnOnePost()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();

			// -- prepare data in db
			/*
			context.Posts.ToList().ForEach
			(
				x => context.Posts.Remove(x)
			);
			context.Posts.Add
			(
				new Post
				{
					Author = "test",
					Content = "test, test, test..."
				}
			);
			context.SaveChanges();
			*/

			// act
			var result = repository.GetAllPosts();

			// assert
			Assert.AreEqual(1, result.Count());
		}

		[TestMethod]
		public void AddOnePost_OneMorePostInDb_ShouldSaveOnePost()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			var validPost = new Post()
			{
				Author = "test_author",
				Content = "This is test post."
			};
			var postCountBefore = repository.GetAllPosts().Count();

			// act
			repository.AddPost(validPost);
			
			// assert
			Assert.AreEqual(postCountBefore + 1, repository.GetAllPosts().Count());
			Assert.AreEqual(validPost.Author, repository.GetAllPosts().Last().Author);
			Assert.AreEqual(validPost.Content, repository.GetAllPosts().Last().Content);
		}

		[TestMethod]
		public void AddNullAuthorInvalidPost_DbWithNoChanges_ShouldRejectInvalidPost()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			var invalidPost = new Post()
			{
				Author = null,
				Content = "This is test post."
			};
			var postCountBefore = repository.GetAllPosts().Count();

			// act
			repository.AddPost(invalidPost);

			// assert
			Assert.AreEqual(postCountBefore, repository.GetAllPosts().Count());
		}

		[TestMethod]
		public void AddEmptyAuthorInvalidPost_DbWithNoChanges_ShouldRejectInvalidPost()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			var invalidPost = new Post()
			{
				Author = "",
				Content = "This is test post."
			};
			var postCountBefore = repository.GetAllPosts().Count();

			// act
			repository.AddPost(invalidPost);

			// assert
			Assert.AreEqual(postCountBefore, repository.GetAllPosts().Count());
		}

		[TestMethod]
		public void AddNullContentInvalidPost_DbWithNoChanges_ShouldRejectInvalidPost()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			var invalidPost = new Post()
			{
				Author = "test_author",
				Content = null
			};
			var postCountBefore = repository.GetAllPosts().Count();

			// act
			repository.AddPost(invalidPost);

			// assert
			Assert.AreEqual(postCountBefore, repository.GetAllPosts().Count());
		}

		[TestMethod]
		public void AddEmptyContentInvalidPost_DbWithNoChanges_ShouldRejectInvalidPost()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			var invalidPost = new Post()
			{
				Author = "test_author",
				Content = ""
			};
			var postCountBefore = repository.GetAllPosts().Count();

			// act
			repository.AddPost(invalidPost);

			// assert
			Assert.AreEqual(postCountBefore, repository.GetAllPosts().Count());
		}

		[TestMethod]
		public void GetAllPostComments_OnePostCommentInDb_ShouldReturnOnePostComment()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			Post post = repository.GetAllPosts().First();

			// act
			var comments = repository.GetAllPostComments(post);

			// assert
			Assert.AreEqual(1, comments.Count());
			Assert.AreEqual(post.Id, comments.First().PostId);
		}

		[TestMethod]
		public void GetAllComments_OnePostCommentInDb_ShouldReturnOnePostComment()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();

			// act
			var result = repository.GetAllComments();

			// assert
			Assert.AreEqual(1, result.Count());
		}

		[TestMethod]
		public void AddValidComment_TwoPostCommentsInDb_ShouldSaveValidPostComment()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			Post post = repository.GetAllPosts().First();
			var comment = new Comment()
			{
				Commented = post,
				Content = "This is another test comment."
			};

			// act
			repository.AddComment(comment);

			// assert
			Assert.AreEqual(2, repository.GetAllComments().Count());
			Assert.AreEqual(comment.Commented, repository.GetAllComments().Last().Commented);
			Assert.AreEqual(comment.Content, repository.GetAllComments().Last().Content);
		}

		[TestMethod]
		public void AddEmptyContentInvalidComment_DbWithNoChanges_ShouldRejectInvalidPostComment()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			Post post = repository.GetAllPosts().First();
			var comment = new Comment()
			{
				Commented = post,
				Content = ""
			};

			// act
			repository.AddComment(comment);

			// assert
			Assert.AreEqual(1, repository.GetAllComments().Count());
		}

		[TestMethod]
		public void AddNullContentInvalidComment_DbWithNoChanges_ShouldRejectInvalidPostComment()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			Post post = repository.GetAllPosts().First();
			var comment = new Comment()
			{
				Commented = post,
				Content = null
			};

			// act
			repository.AddComment(comment);

			// assert
			Assert.AreEqual(1, repository.GetAllComments().Count());
		}

		[TestMethod]
		public void AddNullCommentedPostInvalidComment_DbWithNoChanges_ShouldRejectInvalidPostComment()
		{
			// arrange
			var context = new BlogContext();
			context.Database.CreateIfNotExists();
			var repository = new BlogRepository();
			Post post = repository.GetAllPosts().First();
			var comment = new Comment()
			{
				Commented = null,
				Content = "This is another test comment."
			};

			// act
			repository.AddComment(comment);

			// assert
			Assert.AreEqual(1, repository.GetAllComments().Count());
		}
	}
}
