﻿using System.Collections.Generic;
using Blog.DAL.Infrastructure;
using Blog.DAL.Model;
using System;

namespace Blog.DAL.Repository
{
	public class BlogRepository
	{
		private readonly BlogContext _context;

		public BlogRepository()
		{
			_context = new BlogContext();
		}

		public IEnumerable<Post> GetAllPosts()
		{
			return _context.Posts;
		}

		public bool AddPost(Post post)
		{
			return _context.AddPost(post);
		}

		public IEnumerable<Comment> GetAllComments()
		{
			return _context.Comments;
		}

		public bool AddComment(Comment comment)
		{
			return _context.AddComment(comment);
		}

		public IEnumerable<Comment> GetAllPostComments(Post post)
		{
			return _context.GetAllPostComments(post);
		}
	}
}
