﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog.DAL.Model
{
	public class Comment
	{
		[Key]
		public long Id { get; set; }
		
		public long PostId { get; set; }

		[ForeignKey("PostId")]
		public Post Commented { get; set; }

		public string Content { get; set; }

		public bool IsValid()
		{
			bool isValid = true;

			isValid &= Content != null && Content != "";
			isValid &= Commented != null;

			return isValid;
		}
	}
}
