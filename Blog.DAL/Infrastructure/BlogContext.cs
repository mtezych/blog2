﻿using System.Data.Entity;
using Blog.DAL.Model;
using System.Linq;

namespace Blog.DAL.Infrastructure
{
	public class BlogContext : DbContext
	{
		public IDbSet<Post> Posts { get; set; }
		public IDbSet<Comment> Comments { get; set; }

		public BlogContext()
		:
			base("Blog")
		{
		}

		public bool AddPost(Post post)
		{
			if (post.IsValid())
			{
				Posts.Add(post);
				SaveChanges();
				return true;
			}
			else
			{
				return false;
			}
		}

		internal bool AddComment(Comment comment)
		{
			if (comment.IsValid())
			{
				Comments.Add(comment);
				SaveChanges();
				return true;
			}
			else
			{
				return false;
			}
		}

		internal System.Collections.Generic.IEnumerable<Comment> GetAllPostComments(Post post)
		{
			return Comments.Where(comment => comment.PostId == post.Id);
		}
	}
}
